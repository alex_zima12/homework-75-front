import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {postEncodedWord, postDecodedWord} from "../../store/actions/actions";

const VigenereForm = (props) => {
    const encodedMessage = useSelector(state => state.encodedMessage);
    const decodedMessage = useSelector(state => state.decodedMessage);
    const dispatch = useDispatch();

    const [state, setState] = useState({
        encode: "",
        password: "",
        decode: ""
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    useEffect(() => {
        setState(prevState => {
            return {...prevState, encode: encodedMessage};
        });
    }, [encodedMessage]);

    useEffect(() => {
        setState(prevState => {
            return {...prevState, decode: decodedMessage};
        });
    }, [decodedMessage]);

    const encodeWordsHandler = () => {
        const obj = {
            message: state.decode,
            password: state.password
        }
        dispatch(postEncodedWord(obj))
    };

    const decodeWordsHandler = () => {
        const obj = {
            message: state.encode,
            password: state.password
        }
        dispatch(postDecodedWord(obj))
    };

    return (
        <>
            <form className="container">
                <div className="form-group mx-sm-3 mb-3">
                    <label htmlFor="inputDecoded" className="col-sm-2 col-form-label">Decoded message</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="inputDecoded"
                            name="decode"
                            onChange={inputChangeHandler}
                            value={state.decode}
                        />
                    </div>
                </div>

                <div className="form-inline pl-2 mx-sm-3 mb-2">
                    <div className="form-group mx-sm-3 mb-2">
                        <label htmlFor="inputPassword" className="col-sm-2 col-form-label">Password</label>
                        <div className="col-sm-10">
                            <input
                                type="password"
                                className="form-control"
                                id="inputPassword"
                                name="password"
                                required
                                onChange={inputChangeHandler}
                            />
                        </div>
                    </div>
                    <button
                        type="button"
                        className="btn btn-primary m-2"
                        onClick={encodeWordsHandler}
                    >
                        <span>&dArr;</span>
                    </button>
                    <button
                        type="button"
                        className="btn btn-primary m-2"
                        onClick={decodeWordsHandler}
                    >
                        <span>&uArr;</span>
                    </button>
                </div>
                <div className="form-group mx-sm-3 m-2">
                    <label htmlFor="inputEncoded" className="col-sm-2 col-form-label">Encoded message</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="inputEncoded"
                            name="encode"
                            onChange={inputChangeHandler}
                            value={state.encode}
                        />
                    </div>
                </div>
            </form>
        </>
    );
};

export default VigenereForm;