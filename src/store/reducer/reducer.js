import {POST_ENCODE_SUCCESS,POST_DECODE_SUCCESS} from "../actionsTypes";

const initialState = {
    encodedMessage: '',
    decodedMessage: ''
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case POST_ENCODE_SUCCESS:
            return {...state, encodedMessage: action.value.encoded};
        case POST_DECODE_SUCCESS:
             return {...state, decodedMessage: action.value.decoded};
        default:
            return state;
    }
};

export default reducer;