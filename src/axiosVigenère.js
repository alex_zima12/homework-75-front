import axios from 'axios';

const axiosVigenère = axios.create({
    baseURL: "http://localhost:8000"
});

export default axiosVigenère;