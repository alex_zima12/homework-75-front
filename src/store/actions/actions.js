import axios from "../../axiosVigenère";
import {POST_ENCODE_SUCCESS, POST_DECODE_SUCCESS} from "../actionsTypes";

const postEncodeSuccess = value => {
    return {type: POST_ENCODE_SUCCESS, value};
};

export const postEncodedWord = message => {
    return dispatch => {
        return axios.post("/encode", message).then((response) => {
            dispatch(postEncodeSuccess(response.data));
        });
    };
};

const postDecodedSuccess = value => {
    return {type: POST_DECODE_SUCCESS, value};
};

export const postDecodedWord = message => {
    return dispatch => {
        return axios.post("/decode", message).then((response) => {
            dispatch(postDecodedSuccess(response.data));
        });
    };
};
