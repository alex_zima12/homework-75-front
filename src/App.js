import React from 'react';
import VigenereForm from "./components/VigenereForm/VigenereForm";

function App() {
    return (
        <>
            <nav className="navbar navbar-dark bg-dark mb-5">
                <h1 className="container text-light">Vigenere Cipher</h1>
            </nav>
            <VigenereForm/>
        </>
    );
}

export default App;
